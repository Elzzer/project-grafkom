#include <windows.h>
#include <math.h>
#include <iostream>
#include <GL/gl.h>
#include "glut.h"
#include <Windows.h>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <cstdio>
#include <stdlib.h>


using namespace std;

struct Objek{
	vector< vector<float> > vektor;
	vector< vector<float> > vektorNormal;
	vector< vector<int> > faces;
};

Objek Benda[8];
//Sudut rotasi kamera
float angleX = 0.0f, angleY = 0.0f;
float cameraVectorX = 0.0f, cameraVectorZ = -1.0f, cameraVectorY = 0.0f;
float cameraX = 10.0f, cameraZ = 50.0f, cameraY = 5.0f;
float deltaAngleX = 0.1f, yOrigin = -1, xOrigin = -1, deltaAngleY = 0.1f;
int angle;
//GLfloat angle = 45;


GLfloat diffuseMaterial[4] = { 0.5, 0.5, 0.5, 1.0 };

GLfloat pos1[] = { 29.0,29.0,29.0,0.0 };
GLfloat white[] = { 1,1,1,0 };
GLfloat direction1[] = {50,50,50,0};

GLfloat pos2[] = { 30,29,15,1 };
GLfloat white2[] = { 1,1,1,1 };

int w, h;
GLuint textkursi,textpyramid,textgear,textgelas,textmeja,textaluminium,textlantai,wall2,ceiltex;
GLuint texture[6];
unsigned char texturedata[9][512*512*3];

GLfloat posisiBenda[9][3] = { { 20,14,20 }, { 9.40,2.95,20.65 }, { 9.5,1.44,17.4 }, { 5.5,0.22,20 }, { 28.4,2.5,35 },{20,0,2},{23,3.5,2} ,{20,0,5},{ 21.4,2.5,35 } };

vector<string> split (const string &s, char delim) {
    vector<string> result;
    stringstream ss (s);
    string item;
    while (getline (ss, item, delim)) {
        result.push_back (item);
    }
    return result;
}
vector<string> splitstring (string s, string delimiter) {
    size_t pos_start = 0, pos_end, delim_len = delimiter.length();
    string token;
    vector<string> res;
    while ((pos_end = s.find (delimiter, pos_start)) != string::npos) {
        token = s.substr (pos_start, pos_end - pos_start);
        pos_start = pos_end + delim_len;
        res.push_back (token);
    }
    res.push_back (s.substr (pos_start));
    return res;
}

void LoadBMP(char *fname, unsigned char *buf, int& w, int& h)
{
	HANDLE handle;
	BITMAPFILEHEADER fileheader;
	BITMAPINFOHEADER infoheader;
	unsigned long read;
	unsigned long palettelength;
	RGBQUAD palette[256];
	int width, height;
	unsigned long bitmaplength;
	handle=CreateFile(fname, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, 0, 0);
	if (handle==INVALID_HANDLE_VALUE) exit(1);
	ReadFile(handle, &fileheader, sizeof(fileheader), &read, 0);
	ReadFile(handle, &infoheader, sizeof(infoheader), &read, 0);
	palettelength = infoheader.biClrUsed;
	ReadFile(handle, palette, palettelength, &read, 0);
	if (read!=palettelength) exit(1);
	width = infoheader.biWidth;
	height = infoheader.biHeight;
	bitmaplength = infoheader.biSizeImage;
	if (bitmaplength==0)
		bitmaplength = width*height*infoheader.biBitCount / 8;
	ReadFile(handle, buf, bitmaplength, &read, 0);
	if (read!=bitmaplength) exit(1);

	CloseHandle(handle);
	for(unsigned long i=0;i<bitmaplength;i+=3)
		{
		unsigned char tmp;
		tmp=buf[i];
		buf[i]=buf[i+2];
		buf[i+2]=tmp;
		}
	w=width; h=height;
}

void LoadTexture(char *fn, unsigned char *buf, GLuint& id)
{
	GLuint tex1;
	glEnable(GL_TEXTURE_2D);
	LoadBMP(fn, buf, w, h);
	glGenTextures(1, &tex1);
	id = tex1;
	glBindTexture(GL_TEXTURE_2D, id);
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glTexImage2D(GL_TEXTURE_2D, 0, 3, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, buf);
}

void setupTexture(){
    LoadTexture("kursi.bmp",texturedata[2],textkursi);
    LoadTexture("glass.bmp",texturedata[3],textgelas);
    LoadTexture("pyramid.bmp",texturedata[4],textpyramid);
    LoadTexture("putih.bmp",texturedata[5],textgear);
    LoadTexture("lantaikayu.bmp",texturedata[6],textlantai);
    LoadTexture("newtembok.bmp",texturedata[7],wall2);
    LoadTexture("ceiling2.bmp",texturedata[8],ceiltex);
}

void loadObj(string filename, int index){
    ifstream inFile;
    string line;
    inFile.open(filename.c_str());
    if (!inFile) {
        cout << "Unable to open file";
    }else{
        while (getline(inFile, line)) {
            char kataPertama = line[0];
            if(kataPertama == 'f'){
                line = line.substr(2);
                vector<string> splitted = split(line, ' ');
                vector<string> pisahPertama = splitstring(splitted[0], "//");
                vector<string> pisahKedua = splitstring(splitted[1], "//");
                vector<string> pisahKetiga = splitstring(splitted[2], "//");
                vector<int> face;
                int temp1, temp2;
                stringstream(pisahPertama[0]) >> temp1;
                stringstream(pisahPertama[1]) >> temp2;
                face.push_back(temp1);
                face.push_back(temp2);

                stringstream(pisahKedua[0]) >> temp1;
                stringstream(pisahKedua[1]) >> temp2;
                face.push_back(temp1);
                face.push_back(temp2);

                stringstream(pisahKetiga[0]) >> temp1;
                stringstream(pisahKetiga[1]) >> temp2;
                face.push_back(temp1);
                face.push_back(temp2);
                Benda[index].faces.push_back(face);
                face.clear();
            }else{
                char kataKedua = line[1];
                if(kataKedua == 'n'){
                    line = line.substr(3);
                    vector<float> normal;
                    vector<string> splitted = split(line, ' ');
                    float temp1, temp2, temp3;
                    stringstream(splitted[0]) >> temp1;
                    stringstream(splitted[1]) >> temp2;
                    stringstream(splitted[2]) >> temp3;
                    normal.push_back(temp1);
                    normal.push_back(temp2);
                    normal.push_back(temp3);
                    Benda[index].vektorNormal.push_back(normal);
                    normal.clear();
                }else if(line.substr(0,2) == "v "){
                    line = line.substr(2);
                    vector<float> vertex;
                    vector<string> splitted = split(line, ' ');
                    float temp1, temp2, temp3;
                    stringstream(splitted[0]) >> temp1;
                    stringstream(splitted[1]) >> temp2;
                    stringstream(splitted[2]) >> temp3;
                    vertex.push_back(temp1);
                    vertex.push_back(temp2);
                    vertex.push_back(temp3);
                    Benda[index].vektor.push_back(vertex);
                    vertex.clear();
                }
            }
        }
        inFile.close();
    }
}

void keyDown(unsigned char key, int x, int y){
    float fraction = 0.5f;
    if(key == 27) exit(0);
    else if(key == 'w' || key == 'W'){
        cameraX += cameraVectorX * fraction;
        cameraZ += cameraVectorZ * fraction;
        cameraY += cameraVectorY * fraction;
    }else if(key == 'a' || key == 'A'){
        angleX -= 0.1f;
        cameraVectorX = sin(angleX);
        cameraVectorZ = -cos(angleX);
    }else if(key == 's' || key == 'S'){
        cameraX -= cameraVectorX * fraction;
        cameraZ -= cameraVectorZ * fraction;
        cameraY -= cameraVectorY * fraction;
    }else if(key == 'd' || key == 'D'){
        angleX += 0.1f;
        cameraVectorX = sin(angleX);
        cameraVectorZ = -cos(angleX);
    }

    if(cameraX < 2) cameraX = 2;
    if(cameraY < 2) cameraY = 2;
    if(cameraZ < 2) cameraZ = 2;
    if(cameraX > 28) cameraX = 28;
    if(cameraY > 13) cameraY = 13;
    if(cameraZ > 58) cameraZ = 58;
}

void mouseButton(int button, int state, int x, int y){
    if(button == GLUT_LEFT_BUTTON){
        if(state == GLUT_UP){
            angleY += deltaAngleY;
            angleX += deltaAngleX;
            xOrigin = -1;
            yOrigin = -1;
        }else{
            xOrigin = x;
            yOrigin = y;
        }
    }
}

void mouseMove(int x, int y){
    if(yOrigin >= 0){
        deltaAngleY = (y - yOrigin) * 0.001f;
        cameraVectorY = sin(angleY + deltaAngleY);
    }
}

void DrawWall()
{
	GLfloat c[4];

	//glLightfv(GL_LIGHT0, GL_POSITION, pos1);
	//glLightfv(GL_LIGHT0, GL_DIFFUSE, white);
	//glEnable(GL_LIGHT0);


	c[0] = c[1] = c[2] = c[3] = 1;

	//glMaterialfv(GL_FRONT, GL_AMBIENT, c);
	//ruang tengah
	//depan
	glBindTexture(GL_TEXTURE_2D, wall2);
	glBegin(GL_QUADS);
	//glNormal3f(0,0,1);
	glTexCoord2f(0,0); glVertex3f( 0, 0, 0);
	glTexCoord2f(4,0); glVertex3f(30, 0, 0);
	glTexCoord2f(4,4); glVertex3f(30,15, 0);
	glTexCoord2f(0,4); glVertex3f( 0,15, 0);
	glEnd();
	//belakang
	glBindTexture(GL_TEXTURE_2D, wall2);
	glBegin(GL_QUADS);
	//glNormal3f(0,0,-1);
	glTexCoord2f(0,4); glVertex3f( 20,15,30);
	glTexCoord2f(4,4); glVertex3f(30,15,30);
	glTexCoord2f(4,0); glVertex3f(30, 0,30);
	glTexCoord2f(0,0); glVertex3f( 20, 0,30);
	glEnd();

	//kiri
	glBindTexture(GL_TEXTURE_2D, wall2);
	glBegin(GL_QUADS);
	//glNormal3f(1,0,0);
	glTexCoord2f(0,0); glVertex3f( 0, 0, 0);
	glTexCoord2f(0,4); glVertex3f( 0,15, 0);
	glTexCoord2f(4,4); glVertex3f( 0,15,30);
	glTexCoord2f(4,0); glVertex3f( 0, 0,30);
	glEnd();
	//kanan
	glBindTexture(GL_TEXTURE_2D, wall2);
	glBegin(GL_QUADS);
	//glNormal3f(-1,0,0);
	glTexCoord2f(0,0); glVertex3f( 30, 0,30);
	glTexCoord2f(0,4); glVertex3f( 30,15,30);
	glTexCoord2f(4,4); glVertex3f( 30,15, 0);
	glTexCoord2f(4,0); glVertex3f( 30, 0, 0);
	glEnd();

    //GROUND
    glBindTexture(GL_TEXTURE_2D, textlantai);
	glBegin(GL_QUADS);
	//glNormal3f(0,1,0);
	glTexCoord2f(0,0); glVertex3f( 0,0, 0);
	glTexCoord2f(4,0); glVertex3f(30,0, 0);
	glTexCoord2f(4,4); glVertex3f(30,0,30);
	glTexCoord2f(0,4); glVertex3f( 0,0,30);
	glEnd();

    //ceiling
	glBindTexture(GL_TEXTURE_2D, textkursi);
	glBegin(GL_QUADS);
	//glNormal3f(0,1,0);
	glTexCoord2f(0,0); glVertex3f( 0,15, 0);
	glTexCoord2f(4,0); glVertex3f(30,15, 0);
	glTexCoord2f(4,4); glVertex3f(30,15,30);
	glTexCoord2f(0,4); glVertex3f( 0,15,30);
	glEnd();

    //buat ruangan kanan
    //depan
	//glBindTexture(GL_TEXTURE_2D, wall2);
	//glBegin(GL_QUADS);
	//glNormal3f(0,0,1);
	//glTexCoord2f(0,0); glVertex3f( 30, 0, 0);
	//glTexCoord2f(4,0); glVertex3f(60, 0, 0);
	//glTexCoord2f(4,4); glVertex3f(60,15, 0);
	//glTexCoord2f(0,4); glVertex3f( 30,15, 0);
	//glEnd();
	//belakang
	//glBindTexture(GL_TEXTURE_2D, wall2);
	//glBegin(GL_QUADS);
	//glNormal3f(0,0,-1);
	//glTexCoord2f(0,4); glVertex3f( 50,15,30);
	//glTexCoord2f(4,4); glVertex3f(60,15,30);
	//glTexCoord2f(4,0); glVertex3f(60, 0,30);
	//glTexCoord2f(0,0); glVertex3f( 50, 0,30);
	//glEnd();

	//kanan
	//glBindTexture(GL_TEXTURE_2D, wall2);
	//glBegin(GL_QUADS);
	//glNormal3f(-1,0,0);
	//glTexCoord2f(0,0); glVertex3f( 60, 0,0);
	//glTexCoord2f(0,4); glVertex3f( 60,15,0);
	//glTexCoord2f(4,4); glVertex3f( 60,15, 30);
	//glTexCoord2f(4,0); glVertex3f( 60, 0, 30);
	//glEnd();

    //GROUND
    //glBindTexture(GL_TEXTURE_2D, textlantai);
	//glBegin(GL_QUADS);
	//glNormal3f(0,1,0);
	//glTexCoord2f(0,0); glVertex3f( 30,0, 0);
	//glTexCoord2f(4,0); glVertex3f(60,0, 0);
	//glTexCoord2f(4,4); glVertex3f(60,0,30);
	//glTexCoord2f(0,4); glVertex3f( 30,0,30);
	//glEnd();

    //ceiling
	//glBindTexture(GL_TEXTURE_2D, textkursi);
	//glBegin(GL_QUADS);
	//glNormal3f(0,1,0);
	//glTexCoord2f(0,0); glVertex3f( 30,15, 0);
	//glTexCoord2f(4,0); glVertex3f(60,15, 0);
	//glTexCoord2f(4,4); glVertex3f(60,15,30);
	//glTexCoord2f(0,4); glVertex3f( 30,15,30);
	//glEnd();

    //ruang belakang (toilet)

    //depan
	glBindTexture(GL_TEXTURE_2D, wall2);
	glBegin(GL_QUADS);
	//glNormal3f(0,0,1);
	glTexCoord2f(0,0); glVertex3f( 20, 0, 30);
	glTexCoord2f(4,0); glVertex3f(30, 0, 30);
	glTexCoord2f(4,4); glVertex3f(30,15, 30);
	glTexCoord2f(0,4); glVertex3f( 20,15, 30);
	glEnd();
	//belakang
	glBindTexture(GL_TEXTURE_2D, wall2);
	glBegin(GL_QUADS);
	//glNormal3f(0,0,-1);
	glTexCoord2f(0,4); glVertex3f( 0,15,60);
	glTexCoord2f(4,4); glVertex3f(30,15,60);
	glTexCoord2f(4,0); glVertex3f(30, 0,60);
	glTexCoord2f(0,0); glVertex3f( 0, 0,60);
	glEnd();

	//kiri
	glBindTexture(GL_TEXTURE_2D, wall2);
	glBegin(GL_QUADS);
	//glNormal3f(1,0,0);
	glTexCoord2f(0,0); glVertex3f( 0, 0, 30);
	glTexCoord2f(0,4); glVertex3f( 0,15, 30);
	glTexCoord2f(4,4); glVertex3f( 0,15,60);
	glTexCoord2f(4,0); glVertex3f( 0, 0,60);
	glEnd();
	//kanan
	glBindTexture(GL_TEXTURE_2D, wall2);
	glBegin(GL_QUADS);
	//glNormal3f(-1,0,0);
	glTexCoord2f(0,0); glVertex3f( 30, 0,60);
	glTexCoord2f(0,4); glVertex3f( 30,15,60);
	glTexCoord2f(4,4); glVertex3f( 30,15, 30);
	glTexCoord2f(4,0); glVertex3f( 30, 0, 30);
	glEnd();
    //tembok batas toilet
    glBindTexture(GL_TEXTURE_2D, wall2);
	glBegin(GL_QUADS);
	//glNormal3f(-1,0,0);
	glTexCoord2f(0,0); glVertex3f( 20, 0,50);
	glTexCoord2f(0,4); glVertex3f( 20,15,50);
	glTexCoord2f(4,4); glVertex3f( 20,15, 30);
	glTexCoord2f(4,0); glVertex3f( 20, 0, 30);
	glEnd();
	//pintu toilet
	glBindTexture(GL_TEXTURE_2D, wall2);
	glBegin(GL_QUADS);
	//glNormal3f(0,0,-1);
	glTexCoord2f(0,4); glVertex3f( 20,15,50);
	glTexCoord2f(4,4); glVertex3f(25,15,50);
	glTexCoord2f(4,0); glVertex3f(25, 0,50);
	glTexCoord2f(0,0); glVertex3f( 20, 0,50);
	//glScalef(0,0,2);
	glEnd();

    //GROUND
    glBindTexture(GL_TEXTURE_2D, textlantai);
	glBegin(GL_QUADS);
	//glNormal3f(0,1,0);
	glTexCoord2f(0,0); glVertex3f( 0,0, 30);
	glTexCoord2f(4,0); glVertex3f(30,0, 30);
	glTexCoord2f(4,4); glVertex3f(30,0,60);
	glTexCoord2f(0,4); glVertex3f( 0,0,60);
	glEnd();

    //ceiling
	glBindTexture(GL_TEXTURE_2D, textkursi);
	glBegin(GL_QUADS);
	//glNormal3f(0,1,0);
	glTexCoord2f(0,0); glVertex3f( 0,15, 30);
	glTexCoord2f(4,0); glVertex3f(30,15, 30);
	glTexCoord2f(4,4); glVertex3f(30,15,60);
	glTexCoord2f(0,4); glVertex3f( 0,15,60);
	glEnd();





}

void drawObjek(){
    DrawWall();
    float h1=0, h2=0, h3=1, w1=0, w2=1, w3=1;
    for(int k = 0;k < 8;k++){
        glPushMatrix();
        glTranslatef(posisiBenda[k][0], posisiBenda[k][1], posisiBenda[k][2]);
        //if(k!=0)glTranslatef(posisiBenda[k][0], posisiBenda[k][1], posisiBenda[k][2]);
        //else glTranslatef((posisiBenda[k][0] + angle), posisiBenda[k][1], posisiBenda[k][2]);
        if(k==0){
            glShadeModel(GL_SMOOTH);
            glBindTexture(GL_TEXTURE_2D, textgelas);
            glScalef(0.5,0.5,0.5);
            glRotatef(angle,0,1,0);
            //glRotatef(90,1,0,0);
        }else if(k==1){
            glShadeModel(GL_SMOOTH);
            glBindTexture(GL_TEXTURE_2D, textgelas);
            glScalef(0.23,0.13,0.23);
        }else if(k==2){
            glShadeModel(GL_SMOOTH);
            glBindTexture(GL_TEXTURE_2D, textkursi);
            glScalef(1.33,1.33,1.33);
        }else if(k==3){
            glShadeModel(GL_SMOOTH);
            glBindTexture(GL_TEXTURE_2D, textkursi);
            glScalef(0.28,0.28,0.28);

        }else if(k==4){
            glShadeModel(GL_SMOOTH);
            glBindTexture(GL_TEXTURE_2D, textgear);
            glScalef(0.18,0.18,0.18);
            glRotatef(-90,1,0,0);
            //glRotatef(270,0,1,0);

        }else if(k==5){
            glShadeModel(GL_SMOOTH);
            glBindTexture(GL_TEXTURE_2D, textkursi);
            glScalef(0.05,0.05,0.05);
            glRotatef(-90,1,0,0);
        }else if(k==6){
            glShadeModel(GL_SMOOTH);
            glBindTexture(GL_TEXTURE_2D, textlantai);
            glRotatef(-90,1,0,0);
            glScalef(0.6,0.6,0.6);
        }
        else if(k==7){
            glShadeModel(GL_SMOOTH);
            glBindTexture(GL_TEXTURE_2D, textaluminium);
            glScalef(0.2,0.2,0.2);
        }else if(k==8){
            glShadeModel(GL_SMOOTH);
            glBindTexture(GL_TEXTURE_2D, textlantai);
            glScalef(1,1,1);
            glRotatef(90,1,0,0);
            glRotatef(90,0,1,0);
            glRotatef(90,0,0,1);
            glRotatef(180,0,0,1);
            glRotatef(180,1,0,0);
        }
        for(int i = 1;i < Benda[k].faces.size();i++){
            glColor3f(1.0,1.0,1.0);
            glBegin(GL_TRIANGLES);
                glNormal3f(
                    Benda[k].vektorNormal[Benda[k].faces[i].at(1) - 1].at(0),
                    Benda[k].vektorNormal[Benda[k].faces[i].at(1) - 1].at(1),
                    Benda[k].vektorNormal[Benda[k].faces[i].at(1) - 1].at(2)
                );
                glTexCoord2f(w1, h1);
                glVertex3f(
                    Benda[k].vektor[Benda[k].faces[i].at(0) - 1].at(0),
                    Benda[k].vektor[Benda[k].faces[i].at(0) - 1].at(1),
                    Benda[k].vektor[Benda[k].faces[i].at(0) - 1].at(2)
                );

                glNormal3f(
                    Benda[k].vektorNormal[Benda[k].faces[i].at(3) - 1].at(0),
                    Benda[k].vektorNormal[Benda[k].faces[i].at(3) - 1].at(1),
                    Benda[k].vektorNormal[Benda[k].faces[i].at(3) - 1].at(2)
                );
                glTexCoord2f(w2, h2);
                glVertex3f(
                    Benda[k].vektor[Benda[k].faces[i].at(2) - 1].at(0),
                    Benda[k].vektor[Benda[k].faces[i].at(2) - 1].at(1),
                    Benda[k].vektor[Benda[k].faces[i].at(2) - 1].at(2)
                );

                glNormal3f(
                    Benda[k].vektorNormal[Benda[k].faces[i].at(5) - 1].at(0),
                    Benda[k].vektorNormal[Benda[k].faces[i].at(5) - 1].at(1),
                    Benda[k].vektorNormal[Benda[k].faces[i].at(5) - 1].at(2)
                );
                glTexCoord2f(w3, h3);
                glVertex3f(
                    Benda[k].vektor[Benda[k].faces[i].at(4) - 1].at(0),
                    Benda[k].vektor[Benda[k].faces[i].at(4) - 1].at(1),
                    Benda[k].vektor[Benda[k].faces[i].at(4) - 1].at(2)
                );
            glEnd();
        }
        glPopMatrix();
    }
}

void lighting(void){
   //GLfloat mat_specular[] = { 1.0, 1.0, 1.0, 1.0 };
   //GLfloat light_position[] = { 1.0, 1.0, 1.0, 0.0 };



   //glClearColor (0.0, 0.0, 0.0, 0.0);
   //glShadeModel(GL_SMOOTH);
   //glEnable(GL_DEPTH_TEST);
   //glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER,GL_TRUE);
   //glEnable(GL_LIGHTING);
   //glEnable(GL_LIGHT0);
   //glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, white);
   //glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, mat_specular);
   //glMaterialf(GL_FRONT, GL_SHININESS, 25.0);
   //glLightfv(GL_LIGHT0, GL_POSITION, pos1);
   //glLightfv(GL_LIGHT0,GL_AMBIENT,white);
   //glLightf(GL_LIGHT0,GL_SPOT_CUTOFF,90.00);

   //glColorMaterial(GL_FRONT, GL_AMBIENT);
   //glEnable(GL_COLOR_MATERIAL);


    //glEnable(GL_LIGHT1);
    //glEnable(GL_NORMALIZE);


    //glEnable(GL_DEPTH_TEST);
    //glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER,GL_TRUE);
    //glEnable(GL_LIGHTING);


    glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER,GL_TRUE);
    glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);

	GLfloat posisilampu[]={30,5,50,1};
	GLfloat color[]={1,1,1,1};

	glLightfv(GL_LIGHT0,GL_POSITION,posisilampu);
	glLightfv(GL_LIGHT0,GL_AMBIENT,color);

    GLfloat posisilampu1[]={20,5,-20,1};
	GLfloat color1[]={1,1,1,1};

	glLightfv(GL_LIGHT1,GL_POSITION,posisilampu1);
	glLightfv(GL_LIGHT1,GL_AMBIENT,color1);

    //glLightfv(GL_LIGHT0, GL_POSITION, pos1);

	//glLightfv(GL_LIGHT0, GL_SPECULAR, white);
	//glLightfv(GL_LIGHT0, GL_SPECULAR, white);
    //glLightf(GL_LIGHT0,GL_SPOT_CUTOFF,90.00);
    //glLightfv(GL_LIGHT0,GL_SPOT_DIRECTION,direction1);
    //glEnable(GL_LIGHT0);
}

void render(void) {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glLoadIdentity();
   //glPushMatrix();
   //glTranslatef(pos1[0],pos1[1],pos1[2]);
   //glPopMatrix();








    gluLookAt(
        cameraX, cameraY, cameraZ,
        cameraX + cameraVectorX, cameraY + cameraVectorY, cameraZ + cameraVectorZ,
        0.0f, 1.0f, 0.0f
    );
    lighting();


	//lampu 2
    //glLightfv(GL_LIGHT1, GL_POSITION, pos2);
    //glLightfv(GL_LIGHT1, GL_AMBIENT, white2);



    //Draw ground start section
    //glColor3f(0.9f, 0.9f, 0.9f);
    //glBindTexture(GL_TEXTURE_2D, textaluminium);
    //glBegin(GL_QUADS);
		//glVertex3f(-100.0f, -0.5f, -100.0f);
		//glVertex3f(-100.0f, -0.5f,  100.0f);
		//glVertex3f( 100.0f, -0.5f,  100.0f);
		//glVertex3f( 100.0f, -0.5f, -100.0f);
		//glTexCoord2f(0,4);
		//glVertex3f(-100.0f, -0.5f, -100.0f);
        //glTexCoord2f(4,4);
        //glVertex3f(-100.0f, -0.5f,  100.0f);
        //glTexCoord2f(4,0);
        //glVertex3f( 100.0f, -0.5f,  100.0f);
        //glTexCoord2f(0,0);
        //glVertex3f( 100.0f, -0.5f, -100.0f);
	//glEnd();
	//Draw ground end section

    /*for(int i = 0; i < 3; i++){

    }*/
	//glRotatef(angle,0,1,0);








    drawObjek();



	glutSwapBuffers();
}

void changeSize(int w, int h) {
	if(h == 0) h = 1;
	float ratio = 1.0* w / h;

	glMatrixMode(GL_PROJECTION);

	glLoadIdentity();

	glViewport(0, 0, w, h);

	gluPerspective(45,ratio,1,1000);

	glMatrixMode(GL_MODELVIEW);
}

void StepRot(int n)  //the "glutTimerFunc"
{
	n++;
	angle+=9;
	//glutPostRedisplay();
	glutTimerFunc(50,StepRot,n);
	//if(n<100) glutTimerFunc(1000,StepRot,n);
}

int main(int argc, char **argv){
    string nama[9] = {"fan.obj","glass.obj","meja.obj","kursi.obj","toilet.obj","workdesk.obj","tropifix.obj","workdesk.obj",};
    for(int i = 0;i<7;i++) loadObj(nama[i], i);
    /*for(int i = 0;i<outNormalX.size();i++){
        cout<<outNormalX[i]<<" "<<outNormalY[i]<<" "<<outNormalZ[i]<<endl;
    }*/
    /*for(int i = 0;i < outVertexX.size();i++){
        cout<<outVertexX[i]<<" "<<outVertexY[i]<<" "<<outVertexZ[i]<<endl;
    }*/
    //Glut init section
    glutInit(&argc, argv);
    glutInitWindowSize(600, 600);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
    //Glut create window section
	glutCreateWindow("Yves & Ferdinan");





    //Glut proc set

	glutReshapeFunc(changeSize);
	glutKeyboardFunc(keyDown);
	glutMouseFunc(mouseButton);
    glutMotionFunc(mouseMove);


    glEnable(GL_DEPTH_TEST);
    setupTexture();
	glutDisplayFunc(render);
	glutIdleFunc(render);




    //buat timer buat benda berputar
    glutTimerFunc(50,StepRot,0);

    //Glut loop
	glutMainLoop();
    return 0;
}
